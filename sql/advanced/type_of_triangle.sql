SELECT CASE TRUE
    WHEN A + B > C AND A + C > B AND B + C > A THEN 
        CASE TRUE
            WHEN A = B AND B = C THEN 'Equilateral' 
            WHEN A = B OR A = C OR B = C THEN 'Isosceles' 
            ELSE 'Scalene'
        END
    ELSE 'Not A Triangle' 
    END
FROM TRIANGLES; 
