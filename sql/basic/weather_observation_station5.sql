SELECT station.city , LENGTH(station.city)
FROM station
WHERE LENGTH(station.city) = (
    SELECT MIN(LENGTH(station.city))
    FROM station
    )
ORDER BY station.city
LIMIT 1;

SELECT station.city , LENGTH(station.city)
FROM station
WHERE LENGTH(station.city) = (
    SELECT MAX(LENGTH(station.city))
    FROM station
    )
ORDER BY station.city
LIMIT 1 
