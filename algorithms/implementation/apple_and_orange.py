#!/bin/python3

import sys

def countApplesAndOranges(s, t, a, b, apples, oranges):
    print(len([apple for apple in apples if a + apple >= s and  a + apple <= t]))
    print(len([orange for orange in oranges if b + orange >= s and  b + orange <= t]))
    

if __name__ == "__main__":
    s, t = input().strip().split(' ')
    s, t = [int(s), int(t)]
    a, b = input().strip().split(' ')
    a, b = [int(a), int(b)]
    m, n = input().strip().split(' ')
    m, n = [int(m), int(n)]
    apple = list(map(int, input().strip().split(' ')))
    orange = list(map(int, input().strip().split(' ')))
    countApplesAndOranges(s, t, a, b, apple, orange)