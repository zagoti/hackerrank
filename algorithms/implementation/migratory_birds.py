#!/bin/python3

import sys
import collections

def migratoryBirds(n, ar):
    return collections.Counter(ar).most_common(1)[0][0]

n = int(input().strip())
ar = list(map(int, input().strip().split(' ')))
result = migratoryBirds(n, ar)
print(result) 
