#!/bin/python3

import sys

#return the first 30 numbers as their english equivalent
def firstThirtyNumbers():
    return ["one", 
            "two", 
            "three", 
            "four", 
            "five", 
            "six", 
            "seven", 
            "eight", 
            "nine", 
            "ten", 
            "eleven",
            "twelve",
            "thirteen",
            "fourteen",
            "fifteen",
            "sixteen",
            "seventeen",
            "eighteen",
            "nineteen",
            "twenty",
            "twenty one",
            "twenty two",
            "twenty three",
            "twenty four",
            "twenty five",
            "twenty six",
            "twenty seven",
            "twenty eight",
            "twenty nine",
            "thirty"
           ]

def timeInWords(h, m):
    strings = firstThirtyNumbers()
    hour = strings[h-1]
    if m == 0:
        return hour + " o' clock"
    if m == 15:
        return "quarter past " + hour
    if m == 30:
        return "half past " + hour
    if m == 45:
        return "quarter to " + strings[h]
    if m < 30:
        if m == 1:
            return strings[m-1] + " minute past " + hour
        return strings[m-1] + " minutes past " + hour
    if m > 30:
        return strings[(60-m)-1] + " minutes to " + strings[h]

if __name__ == "__main__":
    h = int(input().strip())
    m = int(input().strip())
    result = timeInWords(h, m)
    print(result)
 
