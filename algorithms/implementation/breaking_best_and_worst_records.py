#!/bin/python3
import sys

def breakingRecords(score):
    minScore = score[0]
    maxScore = score[0]
    minCounter = 0
    maxCounter = 0
    for i in score:
        if i < minScore:
            minScore = i
            minCounter += 1
        elif i > maxScore:
            maxScore = i
            maxCounter += 1
    return maxCounter,minCounter

if __name__ == "__main__":
    n = int(input().strip())
    score = list(map(int, input().strip().split(' ')))
    result = breakingRecords(score)
    print (" ".join(map(str, result)))


 
