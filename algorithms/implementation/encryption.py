#!/bin/python3

import sys
import math


s = input().strip()
rows_size = round(math.sqrt(len(s)))
columns_size = len(s) // rows_size
if columns_size * rows_size < len(s):
    columns_size += 1

for i in range(columns_size):
    for j in range(rows_size):
        idx = j*columns_size+i
        if idx < len(s):
            print(s[idx], end='')
    print(' ', end='')
 
