import sys

def rotateMatrix(m, n, r, matrix):
    result = [[0]*n for x in range(m)]
    for row in range(m):
        for column in range(n):
            minValue = min(m-row-1, row, n-column-1, column)
            maxRow = m-minValue-1
            maxCol = n-minValue-1
            
            resultRow = row
            resultCol = column
            for a in range(r%((maxRow + maxCol) * 2 - minValue * 4)):
                if (resultCol == minValue and resultRow < maxRow):
                    resultRow += 1;
                elif (resultRow == maxRow and resultCol < maxCol):
                    resultCol += 1;
                elif (resultRow > minValue and resultCol == maxCol):
                    resultRow -= 1;
                elif (resultRow == minValue and resultCol > minValue):
                    resultCol -= 1;
            result[resultRow][resultCol] = matrix[row][column];
    return result

def printMatrix(m, n, matrix):
    for i in range(m):
        for j in range(n):
            print(matrix[i][j], end=' ')
        print()

def processInput():
    matrix = []
    for i in range(m):
        matrix.append([int(x) for x in input().strip().split(' ')])
    return matrix


m, n, r = [int(x) for x in input().strip().split(' ')]
data = processInput()
result = rotateMatrix(m, n, r, data)
printMatrix(m, n, result)
