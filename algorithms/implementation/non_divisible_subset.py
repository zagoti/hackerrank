#!/bin/python3

import sys
from collections import Counter


if __name__ == "__main__":
    n, k = input().strip().split(' ')
    n, k = [int(n), int(k)]
    arr = Counter([int(i) % k for i in input().strip().split(' ')])
    result = 0
    for p in range(k // 2 + 1):
        q = (k - p) % k
        if p==q: 
            result += 1 if arr[p] else 0
        else: 
            result += max(arr[p],arr[q])
    print(result)