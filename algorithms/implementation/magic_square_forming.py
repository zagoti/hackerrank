#!/bin/python3

import sys

#returns a list of all matrices that are 3x3 magic squares
def getAllMagicSquares():
    return [[[8, 1, 6], [3, 5, 7], [4, 9, 2]],
             [[6, 1, 8], [7, 5, 3], [2, 9, 4]],
             [[4, 9, 2], [3, 5, 7], [8, 1, 6]],
             [[2, 9, 4], [7, 5, 3], [6, 1, 8]], 
             [[8, 3, 4], [1, 5, 9], [6, 7, 2]],
             [[4, 3, 8], [9, 5, 1], [2, 7, 6]], 
             [[6, 7, 2], [1, 5, 9], [8, 3, 4]], 
             [[2, 7, 6], [9, 5, 1], [4, 3, 8]],]

def formingMagicSquare(s):
    all_changes = []
    for m in getAllMagicSquares():
        all_changes.append(sum([abs(s[i][j]-m[i][j]) for i in range(3) for j in range(3)])) #calculate the cost to transform to magic square m
    return min(all_changes)

if __name__ == "__main__":
    s = []
    for s_i in range(3):
        s_t = [int(s_temp) for s_temp in input().strip().split(' ')]
        s.append(s_t)
    result = formingMagicSquare(s)
    print(result)
