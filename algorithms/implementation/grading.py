#!/bin/python3

import sys

def solve(grades):
    return [(grade if (grade < 38 or grade % 5 < 3) else ((grade // 5 * 5) + 5)) for grade in grades]

n = int(input().strip())
grades = []
grades_i = 0
for grades_i in range(n):
   grades_t = int(input().strip())
   grades.append(grades_t)
result = solve(grades)
print ("\n".join(map(str, result)))