#!/bin/python3

import sys

def organizingContainers(container):
    horizontal_sum = sorted(map(sum,container))
    vertical_sum = sorted(map(sum,zip(*container)))
    for i in range(len(container)):
        if horizontal_sum[i] != vertical_sum[i]:
            return "Impossible"
    return "Possible"


if __name__ == "__main__":
    q = int(input().strip())
    for a0 in range(q):
        n = int(input().strip())
        container = []
        for container_i in range(n):
           container_t = [int(container_temp) for container_temp in input().strip().split(' ')]
           container.append(container_t)
        result = organizingContainers(container)
        print(result)
 
