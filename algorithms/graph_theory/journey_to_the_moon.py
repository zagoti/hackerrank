#!/bin/python3

import sys
sys.setrecursionlimit(5000)

def dfs_rec(adj, vertex, visited, connected):
    visited[vertex] = True
    if vertex not in adj:
        return connected
    for i in adj[vertex]:
        if i not in visited:
            connected = 1 + dfs_rec(adj, i, visited, connected)
    return connected
            
def calculate_graph(adj, astronauts):
    visited = dict()
    connected = 0
    counted = 0
    cost = 0
    for i in range(astronauts):
        if i not in visited:
            connected = dfs_rec(adj, i, visited, 1)
            cost += connected * counted
            counted += connected
    return cost
    
n, p, = [int(x) for x in input().strip().split(' ')]
adj = dict()
for a1 in range(p):
    astronaut_1, astronaut_2 = input().strip().split(' ')
    astronaut_1,astronaut_2 = [int(astronaut_1), int(astronaut_2)]
    if(astronaut_1 in adj):
        adj[astronaut_1].append(astronaut_2)
    else:
        adj[astronaut_1] = [astronaut_1, astronaut_2]
    if(astronaut_2 in adj):
        adj[astronaut_2].append(astronaut_1)
    else:
        adj[astronaut_2] = [astronaut_2, astronaut_1]
print(calculate_graph(adj, n))
 
