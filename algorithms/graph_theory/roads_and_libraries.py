#!/bin/python3

import sys

def dfs_rec(adj, vertex, visited, connected):
    visited[vertex] = True
    if vertex not in adj:
        return connected
    for i in adj[vertex]:
        if i not in visited:
            connected = 1 + dfs_rec(adj, i, visited, connected)
    return connected
            
def calculate_graph_cost(adj, clib, croad, cities):
    cost = 0
    visited = dict()
    connected = 0
    for i in range(1,cities+1):
        if i not in visited:
            connected = dfs_rec(adj, i, visited, 1)
            cost += clib + (connected-1)*croad
    return cost
    
q = int(input().strip())
for a0 in range(q):
    n, m, x, y = input().strip().split(' ')
    n, m, x, y = [int(n), int(m), int(x), int(y)]
    adj = dict()
    for a1 in range(m):
        city_1, city_2 = input().strip().split(' ')
        city_1,city_2 = [int(city_1), int(city_2)]
        if(city_1 in adj):
            adj[city_1].append(city_2)
        else:
            adj[city_1] = [city_1, city_2]
        if(city_2 in adj):
            adj[city_2].append(city_1)
        else:
            adj[city_2] = [city_2, city_1]
            
    if(x <= y or m == 0):
        print(n*x)
    else:
        print(calculate_graph_cost(adj, x, y, n))
