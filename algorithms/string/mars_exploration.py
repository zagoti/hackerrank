#!/bin/python3

import sys


s = input().strip()
s =[s[i:i+3] for i in range(0, len(s), 3)]
errors = 0
for i in s:
    if i[0] != "S":
        errors += 1
    if i[1] != "O":
        errors += 1
    if i[2] != "S":
        errors += 1
print(errors)