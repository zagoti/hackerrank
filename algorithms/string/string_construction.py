#!/bin/python3

import sys

def stringConstruction(s):
    cost = 0
    p = set()
    for i in s:
        if i not in p:
            p.add(i)
            cost += 1
    return cost

if __name__ == "__main__":
    q = int(input().strip())
    for a0 in range(q):
        s = input().strip()
        result = stringConstruction(s)
        print(result) 
