#!/bin/python3

import sys

def gemstones(arr):
    all_elements = dict()
    gems = 0
    for stone in arr:
        for i in set(stone):
            if i not in all_elements:
                all_elements[i] =  1
            else:
                all_elements[i] += 1
    for element,value in all_elements.items():
        if value == len(arr):
            gems += 1
    return gems

n = int(input().strip())
arr = []
arr_i = 0
for arr_i in range(n):
   arr_t = str(input().strip())
   arr.append(arr_t)
result = gemstones(arr)
print(result)
