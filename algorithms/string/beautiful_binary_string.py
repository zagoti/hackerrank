#!/bin/python3

import sys

def minSteps(n, B):
    previous = False
    changes = 0
    i = 0
    while i < n-1:
        if B[i-1:i+2] == "010":
            if not previous:
                previous = True
                changes += 1
                i += 1
            else:
                previous = False
        else:
            previous = False
        i += 1
    return changes

n = int(input().strip())
B = input().strip()
result = minSteps(n, B)
print(result)
