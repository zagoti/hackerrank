#!/bin/python3

import sys

def get_uniform_values(s):
    result = {}
    current = 0
    previous = None
    base_factor = ord("a")-1
    for i in s:
        if i == previous:
            result.update({current:1})
            current += ord(i) - base_factor
        else:
            result.update({current:1})
            current = ord(i) - base_factor
        previous = i
    result.update({current:1})
    return result

s = input().strip()
n = int(input().strip())
uniform_values = get_uniform_values(s)
for a0 in range(n):
    x = int(input().strip())
    if x in uniform_values:
        print("Yes")
    else:
        print("No")
    
