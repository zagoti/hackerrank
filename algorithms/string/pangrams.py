# Enter your code here. Read input from STDIN. Print output to STDOUT
import sys

s = input().strip()
s = s.lower()
alphabet = {}
for element in s:
    if element.isalpha() and element not in alphabet:
        alphabet.update({element:1})
if len(alphabet) == 26:
    print("pangram")
else:
    print("not pangram")