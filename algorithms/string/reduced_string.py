#!/bin/python3

import sys

def super_reduced_string(s):
    reduce = True
    while reduce:
        odd = len(s)%2 == 1
        if odd:
            s1 = s[:-1:2]
            s2 = s[1:-1:2]
            last = [s[-1]]
        else:
            s1 = s[::2]
            s2 = s[1::2]
            last = []
            

        tmp = []
        idx = 0
        length_s1 = len(s1)
        while idx < length_s1:
            if s1[idx] != s2[idx]:
                if idx < length_s1-1 and s1[idx+1] == s2[idx]:
                    tmp.extend([s1[idx],s2[idx+1]])
                    idx += 1
                else:
                    tmp.extend([s1[idx],s2[idx]])    
            idx += 1
        if tmp + last == s:
            reduce = False
        else:
            s = tmp + last
    if s:
        return s
    return "Empty String"

def printList(l):
    [print(i, end='') for i in l]
        

s = input().strip()
result = super_reduced_string(s)
printList(result)