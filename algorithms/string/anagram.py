#!/bin/python3

import sys
import collections

def anagram(s):
    length = len(s)
    if length % 2 == 1:
        return -1
    
    changes = 0
    a = collections.Counter(s[:length//2])
    b = collections.Counter(s[length//2:])
    for char in a.keys():
        if char not in b:
            changes += a[char]
        elif a[char] - b[char] > 0:
            changes += a[char] - b[char]
    return changes

q = int(input().strip())
for a0 in range(q):
    s = input().strip()
    result = anagram(s)
    print(result)
