#!/bin/python3

import sys

def alternatingCharacters(s):
    last = None
    deletions = 0
    for element in s:
        if element == last:
            deletions += 1
        else:
            last = element
    return deletions

q = int(input().strip())
for a0 in range(q):
    s = input().strip()
    result = alternatingCharacters(s)
    print(result)
