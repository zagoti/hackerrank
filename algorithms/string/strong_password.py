#!/bin/python3

import sys

def minimumNumber(n, password):
    special_characters = "!@#$%^&*()-+"
    has_digit = False
    has_lower = False
    has_upper = False
    has_special = False
    requirements = 0
    missing_chars = 0
    missing_requirements = 4
    for letter in password:
        if not has_digit and letter.isdigit():
            has_digit = True
            missing_requirements -= 1
        if not has_lower and letter.islower():
            has_lower = True
            missing_requirements -= 1
        if not has_upper and letter.isupper():
            has_upper = True
            missing_requirements -= 1
        if not has_special and letter in special_characters:
            has_special = True
            missing_requirements -= 1
    missing_chars = 6 - len(password)
    if missing_chars > 0:
        if missing_chars >= missing_requirements:
            return missing_chars
        return missing_chars + abs(missing_chars - missing_requirements)
    return missing_requirements
            
    

if __name__ == "__main__":
    n = int(input().strip())
    password = input().strip()
    answer = minimumNumber(n, password)
    print(answer)
