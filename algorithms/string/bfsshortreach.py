#!/bin/python3

import sys

def distance_calculator(start, edges, queue, n):
    distance = 0
    seen = [-1]*n
    while queue:
        tmp = set()
        for node in queue:
            seen[node] = distance
            for connected in edges[node]:
                if seen[connected] == -1 and connected not in queue:
                    tmp.add(connected)
        queue = tmp
        distance += 6
    return seen
    
def print_distances(start, edges, nodes):
    distances = distance_calculator(start, edges, set([start]), n)
    for i in distances:
        if i != 0:
            print(i, end=" ")

if __name__ == "__main__":
    q = int(input().strip())
    for a0 in range(q):
        n, m = input().strip().split(' ')
        n, m = [int(n), int(m)]
        edges = []
        for a1 in range(n):
            edges.append([])
        for a1 in range(m):
            u, v = input().strip().split(' ')
            u, v = [int(u)-1, int(v)-1]
            edges[u].append(v)
            edges[v].append(u)
        s = int(input().strip())
        print_distances(s-1, edges, n)
        print()