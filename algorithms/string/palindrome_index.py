#!/bin/python3

import sys

def palindromeIndex(s):
    index = -1
    indexFound = 0
    for i in range(len(s)//2):
        if s[i] != s[-(i+1)]:
            index = len(s)-(i+1)
            for j in range(i, len(s)//2): #check if the element in the front has to be removed
                if s[j] != s[-(j+2)]:
                    index = i
                    break
            return index
    return index

q = int(input().strip())
for a0 in range(q):
    s = input().strip()
    result = palindromeIndex(s)
    print(result)
