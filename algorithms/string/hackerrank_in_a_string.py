#!/bin/python3

import sys

search = "hackerrank"
len_search = len(search)
q = int(input().strip())
for a0 in range(q):
    s = input().strip()
    idx = 0
    for i in s:
        if i == search[idx]:
            idx += 1
            if idx == len_search - 1:
                break
    if idx == len_search - 1:
        print("YES")
    else:
        print("NO")
