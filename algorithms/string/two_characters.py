#!/bin/python3
from itertools import combinations
import sys


s_len = int(input().strip())
s = input().strip()
max_length = 0
for i, j in combinations(set(s),2):
    last = None
    length = 0
    possible = True
    for element in s:
        if element == i:
            if last == i:
                possible = False
                break
            else:
                last = i
                length += 1
        if element == j:
            if last == j:
                possible = False
                break
            else:
                last = j
                length += 1
    if possible and length > max_length:
        max_length = length
print(max_length)