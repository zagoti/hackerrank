#!/bin/python3

import sys

def funnyString(s):
    funny = True
    for i in range(len(s)//2 + 1):
        if (abs(ord(s[i]) - ord(s[i+1])) != abs(ord(s[-(i+1)]) - ord(s[-(i+2)]))):
            funny = False
    if funny:
        return "Funny"
    return "Not Funny"

q = int(input().strip())
for a0 in range(q):
    s = input().strip()
    result = funnyString(s)
    print(result)
