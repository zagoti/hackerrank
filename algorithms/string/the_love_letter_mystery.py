#!/bin/python3

import sys

def theLoveLetterMystery(s):
    counter = 0
    for i in range(len(s)//2):
        counter += abs(ord(s[-i-1]) - ord(s[i]))
    return counter

q = int(input().strip())
for a0 in range(q):
    s = input().strip()
    result = theLoveLetterMystery(s)
    print(result)
