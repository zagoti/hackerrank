#!/bin/python3

import sys

n = int(input().strip())
s = input().strip()
k = int(input().strip())
for char in s:
    new_char = char
    if char.islower():
        new_char = ord(char)+(k%26)
        if new_char > ord("z"):
            new_char = chr(new_char-26)
        else:
            new_char = chr(new_char)
    elif char.isupper():
        new_char = ord(char)+(k%26)
        if new_char > ord("Z"):
            new_char = chr(new_char-26)
        else:
            new_char = chr(new_char)
    print(new_char,end='')
