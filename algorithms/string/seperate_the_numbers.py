#!/bin/python3

import sys


q = int(input().strip())
for a0 in range(q):
    s = input().strip()
    first = None
    beautiful = False
    for i in range(1,len(s)//2+1):
        first = int(s[:i])
        test_nr = first
        test_s = str(first)
        while len(test_s) < len(s):
            test_nr += 1
            test_s += str(test_nr)
        if test_s == s:
            beautiful = True
            break
    if beautiful:
        print("YES", first)
    else:
        print("NO")
        
