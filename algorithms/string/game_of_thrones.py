#!/bin/python3

import sys
import collections

def gameOfThrones(s):
    odd_chars = True
    if len(s) % 2 == 1:
        odd_chars = False
        
    s = collections.Counter(s)
    for counter in s.values():
        if counter % 2 == 1:
            if odd_chars == True:
                return "NO"
            odd_chars = True
    return "YES"


s = input().strip()
result = gameOfThrones(s)
print(result)