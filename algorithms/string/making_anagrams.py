#!/bin/python3

import sys
import collections

def makingAnagrams(s1, s2):
    deletions = 0
    diff = collections.Counter(s1)
    diff.subtract(s2)
    for counter in diff.values():
        deletions += abs(counter)
    return deletions


s1 = input().strip()
s2 = input().strip()
result = makingAnagrams(s1, s2)
print(result) 
