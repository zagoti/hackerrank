t1, t2, n = input().strip().split(' ')
t1, t2, n = [int(t1), int(t2), int(n)]

def fib(t1, t2, n):
    if n == 1:
        return 0
    elif n == 2:
        return t2
    return fib(t2, t1 + (t2)**2, n-1)
   
print(fib(t1,t2,n)) 
