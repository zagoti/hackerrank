#!/bin/python3

import sys

def quicksort(arr):
    if len(arr) <= 1: 
        return arr
    else:
        middle = len(arr)//2
        pivot = [x for x in arr if x == arr[middle]]
        smaller = quicksort([x for x in arr if (len(x) == len(arr[middle]) and x < arr[middle]) or len(x) < len(arr[middle])])
        bigger = quicksort([x for x in arr if (len(x) == len(arr[middle]) and x > arr[middle]) or len(x) > len(arr[middle])])
        return smaller + pivot + bigger

n = int(input().strip())
unsorted = []
unsorted_i = 0
for unsorted_i in range(n):
   unsorted_t = str(input().strip())
   unsorted.append(unsorted_t)
sort = quicksort(unsorted)
for i in sort:
    print(i)
