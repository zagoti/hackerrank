import sys

def printList(l):
    for i in l:
        print(i, end=" ")
    print()
    
s = int(input().strip())
ar = [int(x) for x in input().strip().split(" ")]
    
unsorted = True
for idx in range(0,s-1):
    i = idx
    while i >= 0 and ar[i] > ar[i+1]:
        tmp = ar[i+1]
        ar[i+1] = ar[i]
        ar[i] = tmp
        i -= 1   
    printList(ar)
