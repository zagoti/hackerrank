import sys

def printList(l):
    for i in l:
        print(i, end=" ")
    print()
    
n = int(input().strip())
arr = [int(x) for x in input().strip().split(" ")]
    
val = arr[-1]
for idx in range(n-2,-1,-1):
    if arr[idx] >  val:
        arr[idx+1] = arr[idx]
        if idx == 0:
            printList(arr)
            arr[idx] = val
        printList(arr)
    else:
        arr[idx+1] = val
        printList(arr)
        break
