import sys

n,q = [int(x) for x in input().strip().split(' ')]
lastAnswer = 0   
seqList = []
for idx in range(n):
    seqList.append([]) 
    
for a0 in range(q):
    t, x, y = [int(x) for x in input().strip().split(' ')]
    seq = (x ^ lastAnswer) % n
    if t == 1:
        seqList[seq].append(y)
    elif t == 2:
        lastAnswer = seqList[seq][y%len(seqList[seq])]
        print(lastAnswer)
