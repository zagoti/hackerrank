#!/bin/python3

import sys

arr = []
for arr_i in range(6):
   arr_t = [int(arr_temp) for arr_temp in input().strip().split(' ')]
   arr.append(arr_t)

max = 7*(-9)
for intern_row in range(1,5):
    for intern_column in range(1,5):
        sum = 0
        for i in range(-1, 2):
            sum += arr[intern_row-1][intern_column+i]
            
        sum += arr[intern_row][intern_column]
        
        for i in range(-1, 2):
            sum += arr[intern_row+1][intern_column+i]
            
        if sum > max:
            max = sum
print(max)
